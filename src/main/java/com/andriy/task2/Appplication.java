package com.andriy.task2;

import com.andriy.task1.model.Model;
import com.andriy.task2.model.Command;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Appplication {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        CreateCommand lambda = (name, ar)->new Command(name, ar);
            String name = "lambda";
            String ar = "1";


        CreateCommand referens = Command::new;
        name = "referens";
        ar = "2";


        CreateCommand anonimus = new CreateCommand() {
            @Override
            public Command create(String name, String ar) {
                Command commandan = new Command("anonim", "3"    );
                return commandan;
            }
        };


        Command command = new Command("command", "4");
        ;


        System.out.println("Input name :");
        name = scanner.nextLine();
        if (name.equals("lambda")){
            System.out.println(lambda.create(name, ar));
        } else if (name.equals("referens")) {
            System.out.println(referens.create(name, ar));
        } if (name.equals("anonim")) {
            System.out.println(anonimus.create(name, ar));
        } else if (name.equals("command")){
            System.out.println(command);
        } else {
            System.out.println("Input correct name.");
        }


    }
}

@FunctionalInterface
interface CreateCommand {
    public Command create(String name, String ar);
}