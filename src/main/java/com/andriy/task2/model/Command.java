package com.andriy.task2.model;

public class Command {
    private String name;
    private String ar;

    public Command(String name, String ar) {
        this.name = name;
        this.ar = ar;
    }

    public String getName() {
        return name;
    }

    public Command setName(String name) {
        this.name = name;
        return this;
    }

    public String getAr() {
        return ar;
    }

    public Command setAr(String ar) {
        this.ar = ar;
        return this;
    }

    @Override
    public String toString() {
        return "Command{" +
                "name='" + name + '\'' +
                ", ar='" + ar + '\'' +
                '}';
    }
}
