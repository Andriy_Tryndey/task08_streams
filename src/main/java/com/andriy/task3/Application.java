package com.andriy.task3;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Application {

    public static void main(String[] args) {
        Random rand = new Random();

        List<Integer> stream1 = Stream.iterate(1, n -> rand.nextInt(10))
                .limit(5)
                .collect(Collectors.toList());
        System.out.println(stream1);
        System.out.println("max number stream1 = " + stream1.stream().max(Comparator.naturalOrder()).get());
        System.out.println("min number stream1 = " + stream1.stream().min(Comparator.naturalOrder()).get());
        System.out.println("min number stream1 = " + stream1.stream().min(Integer::compare).get());
        System.out.println("Count average stream1 = " + (double)stream1.stream().reduce(0,(a, b) -> a + b)/stream1.stream().count());
        double y1 = (double)stream1.stream().reduce(0,(a, b) -> a + b)/stream1.stream().count();
        System.out.println("sum of list values stream1 = " + stream1.stream().reduce(0,(a, b) -> a + b));
        List<Integer> collect = stream1.stream().filter(i -> i > y1).collect(Collectors.toList());
        System.out.println(collect);

        List<Integer> stream2 = Stream.iterate(1, n -> rand.nextInt(10))
                .limit(7)
                .collect(Collectors.toList());
        System.out.println(stream2);
        System.out.println("max number stream2 = " + stream2.stream().max(Comparator.naturalOrder()).get());
        System.out.println("min number stream2 = " + stream2.stream().min(Comparator.naturalOrder()).get());
        System.out.println("min number stream2 = " + stream2.stream().min(Integer::compare).get());
        double y2 = stream2.stream().reduce(0,(a, b) -> a + b)/stream2.stream().count();
        System.out.println("Count average stream2 = " + (double)stream2.stream().reduce(0,(a, b) -> a + b)/stream2.stream().count());
        System.out.println("sum of list values stream2 = " + stream2.stream().reduce(0,(a, b) -> a + b));
        List<Integer> collect2 = stream2.stream().filter(i -> i > y2).collect(Collectors.toList());
        System.out.println("Count number of values that are bigger than average = " + collect2.size());


        List<Integer> stream3 = Stream.iterate(1, n -> rand.nextInt(10))
                .limit(10)
                .collect(Collectors.toList());
        System.out.println(stream3);
        System.out.println("max number stream3 = " + stream3.stream().max(Comparator.naturalOrder()).get());
        System.out.println("min number stream3 = " + stream3.stream().min(Comparator.naturalOrder()).get());
        System.out.println("min number stream3 = " + stream3.stream().min(Integer::compare).get());
        double y3 = stream3.stream().reduce(0,(a, b) -> a + b)/stream3.stream().count();
        System.out.println("Count average stream3 = " + (double)stream3.stream().reduce(0,(a, b) -> a + b)/stream3.stream().count());
        System.out.println("sum of list values stream3 = " + stream3.stream().reduce(0,(a, b) -> a + b));
        System.out.println(stream3.stream().filter(i -> i > y3).collect(Collectors.toList()).size());



    }
}
