package com.andriy.task1;

import com.andriy.task1.model.Model;

public class Application {
    public static void main(String[] args) {
        Model lambMax = (a, b, c)->{if(a>b && a>b)return a;if (b>a && b>c)return b;else return c;};
        Model lamEverage = (a,b,c)->{if (a>b && a<c)return a;if (b>a && b<c)return b;else return c;};

        System.out.println("Max number1 = " + lambMax.method(2,4,6));
        System.out.println("Max number2 = " + lambMax.method(5,1,4));
        System.out.println("Max number3 = " + lambMax.method(6,6,7));
        System.out.println("Max number4 = " + lambMax.method(3,8,1));
        System.out.println("Everage number1 = " + lamEverage.method(2,4,6));
        System.out.println("Everage number2 = " + lamEverage.method(5,1,4));
        System.out.println("Everage number3 = " + lamEverage.method(6,6,7));
        System.out.println("Everage number4 = " + lamEverage.method(3,8,1));
    }
}
