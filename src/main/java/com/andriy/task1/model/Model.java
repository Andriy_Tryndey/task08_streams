package com.andriy.task1.model;

@FunctionalInterface
public interface Model {
    int method(int one, int two, int three);
}
