package com.andriy.task4.view;

@FunctionalInterface
public interface Printable {
    void print();
}
