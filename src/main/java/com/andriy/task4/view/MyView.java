package com.andriy.task4.view;

import com.andriy.task4.view.Printable;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private Map<String, String> menu;
    private Map<String, Printable> methodMenu;
    Scanner sc = null;

    public MyView(){
        menu = new LinkedHashMap<>();
        menu.put("1", "1. Command as lambda.");
        menu.put("2", "2. Method reference.");
        menu.put("3", "3. Anonymous class.");
        menu.put("4", "4. Object of command class.");
        menu.put("Q", "Q");

        methodMenu = new LinkedHashMap<>();
        methodMenu.put("1", this::pressButton1);
        methodMenu.put("2", this::pressButton2);
        methodMenu.put("3", this::pressButton3);
        methodMenu.put("4", this::pressButton4);
    }

    private void pressButton1() {
        String a = "N1";
        Printable lambds = ()-> System.out.println("Command as lambda." + a);
        lambds.print();
    }

    private void pressButton2() {
        System.out.println("2");
    }

    private void pressButton3() {
        Printable anonim = new Printable() {
            @Override
            public void print() {
                String n = "N3";
                System.out.println("Anonymous class." + n);
            }
        };
        anonim.print();
    }

    private void pressButton4() {
        System.out.println("4");
    }

    public void show2(){
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = sc.nextLine().toLowerCase();
                try {
                    methodMenu.get(keyMenu).print();
                } catch (Exception e) {
            }
        }while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        System.out.println("\nMenu");
        for (String s : menu.values()) {
            System.out.println(s);
        }
    }
}

